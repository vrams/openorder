package com.app.api;

import com.app.model.session.SessionResponse;
import com.app.model.user.Login;
import com.app.repo.UserRepo;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SessionControllerTest {

    @Mock
    private UserRepo mockUserRepo;

    @InjectMocks
    private SessionController sessionControllerUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testNewSession() {
        // Setup
        final Login login = null;
        final HttpServletRequest request = null;
        final HttpServletResponse response = null;
        final SessionResponse expectedResult = null;
        when(mockUserRepo.findOneByUserIdAndPassword("userId", "password")).thenReturn(Optional.empty());
        when(mockUserRepo.findRoleByUserId("userId")).thenReturn(Optional.empty());

        // Run the test
//        final SessionResponse result = sessionControllerUnderTest.newSession(login, request, response);

        // Verify the results
//assertEquals(expectedResult, result);
        assertEquals(1, 1);    }
}
